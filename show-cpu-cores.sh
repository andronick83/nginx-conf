#!/bin/bash

# Show the number of CPU cores and sockets on your system
# https://www.ibm.com/developerworks/mydeveloperworks/blogs/brian/entry/linux_show_the_number_of_cpu_cores_on_your_system17?lang=en

echo "Number of CPU cores:"
cat /proc/cpuinfo  | grep processor

echo "Number of phisical cores:"
cat /proc/cpuinfo | grep "physical id" | sort | uniq | wc -l

echo "Physical core <=> CPU core:"
cat /proc/cpuinfo | egrep "core id|physical id"

echo "Show the number of cores on a system:"
cat /proc/cpuinfo | egrep "core id|physical id" | tr -d "\n" | sed s/physical/\\nphysical/g | grep -v ^$ | sort | uniq | wc -l
