
listen                  *:80;

include                 /var/www/nginx/vapp-nginx-backend.conf;
#include                 /var/www/nginx/vapp-nginx-app-pagespeed.conf;

access_log              /var/www/logs/nginx-$app_name-access.log main;
error_log               /var/www/logs/nginx-$app_name-error.log notice;

add_header              "X-UA-Compatible" "IE=Edge,chrome=1";

# Cross domain AJAX requests
#add_header              "Access-Control-Allow-Origin" "*";

# protect hidden system files
location ~* (^|/)\. {
    return              403;
}

# protect backup/config/source files
location ~* (\.(bak|config|sql|fla|psd|ini|log|sh|inc|swp|dist|sass)|~)$ {
    return              403;
}

# other configurations
index                   index.php index.htm index.html;
charset                 utf-8;
#rewrite_log             on;
#log_not_found           off;
#log_subrequest          on;
#sendfile                on;
#keepalive_timeout       75 60;
#tcp_nopush              on;
#tcp_nodelay             on;
#reset_timedout_connection on;
#server_name_in_redirect on;
client_max_body_size    5m;
client_body_buffer_size 128k;

open_file_cache         max=1000 inactive=5s;
open_file_cache_valid   5s;
open_file_cache_min_uses 2;
open_file_cache_errors  on;

#

# built-in filename-based cache busting
location ~* (.+)\.(\d+)\.(js|css|png|jpg|jpeg|gif)$ {
    try_files           $uri $1.$3; # route all requests for /css/style.20122112.css to /css/style.css
}

# route all *.php files to php-fpm engine
include                         /var/www/nginx/vapp-nginx-php.conf;

# default files
location = /favicon.ico {
    allow                       all;
    log_not_found               off;
    access_log                  off;
    expires                     90d;
    add_header Cache-Control    "public";
}
location = /robots.txt {
    allow                       all;
    log_not_found               off;
    access_log                  off;
    add_header Cache-Control    "public";
}

# cached directories
location ~* /(js|css|img)/ {
    access_log                  off;
    expires                     90d;
    add_header Cache-Control    "public";
}
# cached files
location ~* \.(swf|js|css|png|jpg|jpeg|gif|ico|txt)$ {
    access_log                  off;
    expires                     90d;
    add_header Cache-Control    "public";
}
location ~* \.(nrg|htm|html|jpg|jpeg|gif|png|ico|css|zip|7z|tgz|gz|rar|bz2|doc|xls|exe|pdf|ppt|txt|tar|mid|midi|wav|bmp|rtf|js|avi|mp3|mp4|mpg|iso|djvu|dmg|flac|r70|mdf|chm|sisx|sis|flv|thm|bin)$ {
    expires                     30d;
}

#

location ^~ /static/ {
    rewrite                     ^/static/(.*)$ /$1 break;
    root                        /var/www/html/static;
    autoindex                   off;
    access_log                  off;
    #error_log                  off;
    rewrite_log                 off;
    expires                     90d;
    add_header Cache-Control    "public";
}

location ^~ /private/ {
    autoindex                   on;
    
    satisfy                     any;
    allow                       92.249.90.130/32;
    deny                        all;
    
    auth_basic                  "Restricted";
    auth_basic_user_file        /var/www/.private.passwd;
    
    location /private/nginx-status {
        stub_status             on;
        access_log              off;
        break;
    }
    
    location /private/fpm-status {
        access_log              off;
    }
    
    location /private/fpm-ping {
        access_log              off;
    }
    
    include                     /var/www/nginx/vapp-nginx-php.conf;
}

#

error_page 401 = @denied;
location @denied {
    internal;
    return                      401 "Unauthorized";
}

error_page 500 502 503 504 = @error;
location = @error {
    internal;
    return                      500 "Server Error";
}

error_page 404 = @fallback;
location @fallback {
    internal;
    return                      404 "Not Found";
}

error_page 503 /maintenance.html;
location = /maintenance.html { break; }
if ( -f $document_root/maintenance.html ) { return 503; }

#return 503; # If sevice maintenance
